@extends('layouts.app')
@section('content')

    @if(!empty($errors))
        @include('errors.errors')
    @endif

    <form id="editForm" method="post" action="/edit">
        <div class="modal-body">


            <div class="form-group">
                <label for="task_body" class="col-form-label">Message:</label>
                <textarea class="form-control" id="task_body" name="task_body"
                          required>{{ $task->task_body }}</textarea>
            </div>
            <div class="form-group">
                <label for="task_status" class="col-form-label">Task Status:</label>
                <input id="task_status" class="btn-group-toggle" name="task_status" type="checkbox"
                       @if($task->done) checked @endif autocomplete="off">
                <span id="task_status_text" @if($task->done) class="badge badge-success">
                    DONE @else  class="badge badge-warning"> IN WORK @endif
                </span>


            </div>


        </div>
        <div class="modal-footer">
            <input type="hidden" value="{{ $task->id }}" name = "task_id">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>

@endsection