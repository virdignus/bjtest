@extends('layouts.app')
@section('content')

    @if(!empty($errors))
        @include('errors.errors')
    @endif

    @if($success)
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{ $success }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    @endif
    <div class="list-group mt-3">
    @include ('layouts.sort-panel')

        @foreach($tasks as $task)
            <div
                    class="list-group-item list-group-item-action flex-column align-items-start @if( $task->done) disabled @endif ">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">user: {{ $task->user_name }}
                        <small>({{ $task->user_email }})</small>
                    </h5>
                    <small>Task status : @if($task->done) <span class="badge badge-success ">DONE</span> @else <span
                                class="badge badge-warning ">IN WORK</span> @endif .
                    </small>
                </div>

                <div class="card-body row align-items-center">
                    <div class="col"><img src="{{ $task->task_image }}"></div>
                    <div class="col">

                        <p class="mb-1 pl-3">{{ $task->task_body }}</p>
                    </div>
                </div>
                @if($user)
                    <div class="card-footer">
                        <a href="/edit?id={{ $task->id }}" class="btn btn-primary">Edit</a>
                    </div>
                @endif

            </div>
        @endforeach
    </div>
    @include('layouts.pagination')


@endsection
