<div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="addTaskModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AddTaskModalLabel">New task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addForm" method="post" action="/add" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user_name" class="col-form-label">Your name</label>
                        <input type="text" class="form-control" id="user_name" name="user_name" required>
                    </div>

                    <div class="form-group">
                        <label for="user_email" class="col-form-label">Your E-mail</label>
                        <input type="email" class="form-control" id="user_email" name="user_email" required>
                    </div>

                    <div class="form-group">
                        <label for="task_image" class="col-form-label">Image for task</label>
                        <input type="file" class="form-control" id="task_image" name="task_image">
                    </div>

                    <div class="form-group">
                        <label for="task_body" class="col-form-label">Message:</label>
                        <textarea class="form-control" id="task_body" name="task_body" required></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="previewBtn" type="button" class="btn btn-primary">Preview</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
            <div id="preview" class="modal-body">
                <div class="list-group mt-3">
                    <a href="#"
                       class="list-group-item list-group-item-action flex-column align-items-start ">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1 user_name">user: user</h5>
                            <small class="user_email">email</small>
                        </div>
                        <img src="https://via.placeholder.com/160x120?text=your+image"
                             class="rounded float-left" style="width: 220px;height: 140px;" alt="...">
                        <p class="mb-1 task_body"></p>

                        <small>Task status : <span
                                    class="badge badge-warning ">IN WORK</span>
                        </small>
                    </a>
                </div>
                <a href="#" class="hide-link">Hide preview</a>
            </div>
        </div>


    </div>


</div>