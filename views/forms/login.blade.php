<div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Login Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/login" name="loginForm">
            <div class="modal-body">

                    <div class="form-group">
                        <label for="login">You login</label>
                        <input type="text" class="form-control" id="login" name="login" aria-describedby="loginHelp"
                               placeholder="Enter login">
                        <small id="loginHelp" class="form-text text-muted">Enter you login please.
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>


            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Log in</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>