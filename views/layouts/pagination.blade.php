<nav aria-label="...">
    <ul class="pagination pagination-md">
        @for($page =0;$page<$pages; $page++)

            <li class="page-item @if($_GET['page'] == ($page+1) or ($page == 0 and !isset($_GET['page'])) ) disabled @endif">
                <a class="page-link"
                   href="@if($page != 0)
                           /?page={{ $page+1 }}@if(isset($_GET['sortBy']))&sortBy={{$_GET['sortBy']}}  @endif
                   @else
                           /@if(isset($_GET['sortBy']))&sortBy={{$_GET['sortBy']}}  @endif
                   @endif
                           " tabindex="-1">{{ $page+1 }} </a>
            </li>

        @endfor

    </ul>
</nav>