<nav class="navbar sticky-top navbar-dark bg-dark">
    <a class="navbar-brand text-success" href="/"><i class="fas fa-tasks"></i> Tasks</a>

    <div class="form-inline">
        <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#addTaskModal">Add new task</button>

        @if($user)
            <a href="/logout" type ="button" class="btn btn-sm ml-2 btn-outline-warning" ><i class="fa fa-user"></i> {{ $user->login }}(log out)</a>
        @else
            <button type ="button" class="btn btn-sm ml-2 btn-outline-warning" data-toggle="modal" data-target="#loginForm">Login</button>
        @endif
    </div>

</nav>