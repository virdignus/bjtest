<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <ul class="list-unstyled">
        @foreach($errors as $error)
            <li>
                <strong> Error! </strong> {{ $error }}
            </li>
        @endforeach
    </ul>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>