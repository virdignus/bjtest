$("#previewBtn").on('click', function () {
    $("#preview").show();
    $(".user_name").text("user: " + $("#user_name").val());
    $(".user_email").text($("#user_email").val());
    $(".task_body").text($("#task_body").val());

});
$(".hide-link").on('click', function () {
    $("#preview").hide();
});


$('#task_status').click(function () {
    var statusText = $("#task_status_text");
    var status = $(this).prop("checked")
    if (status) {
        statusText.text("DONE");

    }
    else {
        statusText.text("IN WORK");
    }
    statusText.toggleClass(" badge-warning badge-success");
})
