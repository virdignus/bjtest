<?php
    ini_set('display_errors', 0);
    require dirname(__DIR__) . '/vendor/autoload.php';


    $app = new \Core\Application();
    $app->make();
