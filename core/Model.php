<?php

    namespace Core;

    use PDO;

    class Model extends PDO
    {
        protected $host;
        protected $dbname;
        private $user;
        private $password;
        protected $config;
        protected $dns;
        protected $table;
        protected $classNamespace;
        protected $fillable;
        protected $attributes;


        public function __construct($option = null)
        {
            $this->config = include dirname(__DIR__) . "/config/database.php";
            $this->host = $this->config['host'];
            $this->dbname = $this->config['dbname'];
            $this->user = $this->config['user'];
            $this->password = $this->config['password'];
            $this->dns = $this->getDns();

            parent::__construct($this->dns, $this->user, $this->password, $option);

        }

        /**
         * Get prepared dsn string for connection
         * @return string
         */
        private function getDns()
        {
            return "mysql:host=$this->host;dbname=$this->dbname;charset=UTF8";

        }

        /**
         * Get all records from table
         * @param array $columns list of columns that needed to return
         * @return array of objects
         */
        public function all($columns = ['*'])
        {

            $columnsList = implode(",", $columns);
            $sql = "select $columnsList from $this->table";

            return $this->query($sql)
                ->fetchAll(PDO::FETCH_CLASS, $this->classNamespace . "\\" . $this->getClassname());
        }

        /**
         * Find record by id field
         * @param $id value to be found
         * @return mixed empty array | object
         */
        public function byId($id)
        {
            $array = $this->query("select * from $this->table where id = $id ")
                ->fetchAll(PDO::FETCH_CLASS, $this->classNamespace . "\\" . $this->getClassname());

            return array_shift($array);
        }

        /**
         * select data with custom where clause
         * @param string $whereClause
         * @example $this->>wehere("name = 'Joe Doe' and status = true"
         * @return mixed empty array | object
         */
        public function where(string $whereClause)
        {
            $sql = "select * from $this->table where $whereClause";
            $result = $this->query($sql)
                ->fetchAll(PDO::FETCH_CLASS, $this->classNamespace . "\\" . $this->getClassname());

            return array_shift($result);
        }

        /**
         * Find records
         * @param $column column for search
         * @param $operation can be "=" ">" "<" "<>" "<=" ">="
         * @param $value
         * @return array of objects
         */
        public function find($column, $operation, $value)
        {
            $sql = "select * from $this->table where $column $operation '$value'";

            return $this->query($sql)
                ->fetchAll(PDO::FETCH_CLASS, $this->classNamespace . "\\" . $this->getClassname());
        }

        /**
         * Paginate query
         * @param $limit for selection
         * @param $page offset value
         * @param string $orderBy order collumn
         * @param string $order_type order type
         * @return array of objects
         */
        public function paginate($limit, $page = 0, $orderBy = 'id', $order_type = 'DESC')
        {
            $offset = 0;

            if ((integer)$page == 2) {
                $offset = $limit;
            }

            if ((integer)$page > 2) {
                $offset = $limit * ($page - 1);
            }

            $sql = "select * from $this->table order by $orderBy $order_type limit $offset,$limit ";


            return $this->query($sql)
                ->fetchAll(PDO::FETCH_CLASS, $this->classNamespace . "\\" . $this->getClassname());
        }

        /**
         * Get total count pages for pagination
         * @param $perPage records per page
         * @return int pages count
         */
        public function pagesCount($perPage)
        {
            $items = count($this->all(['id']));
            $inaccuracy = ($items % $perPage) ? 1 : 0;

            return intdiv($items, $perPage) + $inaccuracy;
        }

        /**
         * Insert new recort to table
         * @return int,
         */
        public function insert()
        {
            $columns = implode(",", $this->fillable);
            $values = $this->prepareDataForInsert();
            $sql = "insert into $this->table ($columns) values ($values)";

            return $this->exec($sql);
        }

        /**
         * update record
         * @param array $data
         * @return int
         */
        public function update(array $data)
        {
            $updatedValues = [];
            $sql = "update $this->table set ";
            foreach ($data as $key => $value) {
                $updatedValues[] = "$key = '$value'";
            }
            $sql = $sql . implode(",", $updatedValues) . " where id = $this->id";

            return $this->exec($sql);
        }

        /**
         * Return curent class name
         * @return string
         */
        public function getClassname()
        {
            return class_basename($this);
        }


        /**
         * Prepare data for insert query
         * @return string prepared data for query
         */
        private function prepareDataForInsert()
        {
            $values = [];
            foreach ($this->fillable as $item) {
                $values[] = (string)$this->{$item};
            }

            return "'" . implode("','", $values) . "'";
        }
    }