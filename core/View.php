<?php

    namespace Core;

    use Jenssegers\Blade\Blade;

    class View
    {
        /**
         * @param $view view's name
         * @param array $data  additional data
         * @return string reddered view
         */

        public static function view($view, $data = [])
        {
            $viewPath = Application::config("app","views.view_path");
            $cachePath = Application::config("app","views.cache_path");
            $blade = new Blade($viewPath,$cachePath);
            return (string)$blade->render($view, $data);

        }

        /**
         * @param string $view for 404 error
         */
        public static function pageNotFound($view = 'errors.404')
        {
            $viewPath = Application::config("app","views.view_path");
            $cachePath = Application::config("app","views.cache_path");
            $blade = new Blade($viewPath,$cachePath);
            http_response_code(404);
            echo (string)$blade->render($view);

        }


    }