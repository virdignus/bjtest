<?php

    namespace Core;


    use Illuminate\Support\Arr;
    use Intervention\Image\Exception\ImageException;
    use Intervention\Image\ImageManagerStatic as Image;


    class Application
    {


        private $route;
        const DEFAULT_IMAGE = "/images/default.jpg";

        public function __construct()
        {
            $this->route = new Route();

        }

        public function make()
        {
            $this->route->runRoute();

        }

        /**
         * @param $file
         *
         * @param $key
         * @return mixed
         */
        public static function config($file, $key)
        {
            $config = include dirname(__DIR__) . "/config/$file.php";

            return Arr::get($config, $key);

        }

        public static function getBasePath()
        {
            return dirname(__DIR__) . "/";
        }

        public static function uploadImage($saveDir, $input_image)
        {
            try {

                $img = Image::make($_FILES[$input_image]['tmp_name']);
            } catch (ImageException $exception) {
                return null;
            };
            $imageFolder = self::getBasePath() . "/public/images/";
            $userdir = $imageFolder . $saveDir . "/";

            if (!file_exists($userdir)) {
                mkdir($userdir);
            }

            // resize image
            $extansion = pathinfo($_FILES[$input_image]['name'])['extension'];
            $img->fit(340, 220);
            $fileName = date('U') . "." . $extansion;
            $img->save($userdir . $fileName);

            return "/images/" . $saveDir . "/" . $fileName;

        }


    }