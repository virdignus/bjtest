<?php

    namespace Core;

    use function PHPSTORM_META\elementType;

    class Route
    {
        private $routes;
        private $controllersNamespace;

        public function __construct()
        {
            $this->routes = include dirname(__DIR__) . '/config/routes.php';
            $this->controllersNamespace = "App\\Controllers\\";
        }


        /**
         * Get request, check url in route file and run related action
         * or return error 404
         */
        public function runRoute()
        {
            $method = strtolower($_SERVER['REQUEST_METHOD']);

            $uri = $this->removeQueryStringVariables($_SERVER['REQUEST_URI']);
            if (array_key_exists($uri, $this->routes[$method])) {
                $this->executeController($this->routes[$method][$uri]);
            } else {
                View::pageNotFound();
            }
        }

        /**
         * @param $controller Controller name for execute
         */
        private function executeController($controller)
        {
            call_user_func($this->controllersNamespace . $controller);
        }

        /**
         * For redirect to index page
         */
        public static function redirectToIndexPage()
        {
            header("Location: http://" . $_SERVER['HTTP_HOST']);
        }

        /**
         * separating parameters from url
         * @param $url  Url for separating
         * @return string url without params
         */
        private function removeQueryStringVariables($url)
        {
            if ($url != '') {
                $url = str_replace("?", "&", $url);
                $parts = explode('&', $url, 5);

                if (strpos($parts[0], '=') === false) {
                    $url = $parts[0];
                } else {
                    $url = '';
                }
            }

            return $url;
        }
    }