<?php
    /**
     * Routes list
     */

    return [
        'get'  => [
            '/'       => 'IndexController::index',
            '/logout' => 'AuthController::logout',
            '/edit'   => 'TaskController::show',


        ],
        'post' => [
            '/add'   => 'TaskController::store',
            '/login' => 'AuthController::login',
            '/edit'   => 'TaskController::update',
        ],

    ];