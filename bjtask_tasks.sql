CREATE TABLE tasks
(
    id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_name varchar(100) NOT NULL,
    user_email varchar(50) NOT NULL,
    task_body text NOT NULL,
    task_image varchar(255),
    done tinyint(4) DEFAULT '0'
);
