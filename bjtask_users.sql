CREATE TABLE users
(
    id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    login varchar(50) NOT NULL,
    password varchar(100) NOT NULL,
    session_id varchar(255)
);
CREATE UNIQUE INDEX users_name_uindex ON users (login);
CREATE UNIQUE INDEX users_session_id_uindex ON users (session_id);
INSERT INTO users (id, login, password, session_id) VALUES (1, 'admin', '202cb962ac59075b964b07152d234b70', '');