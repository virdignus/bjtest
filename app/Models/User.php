<?php

    namespace App\Models;


    use Core\Auth;

    class User extends Auth
    {
        protected $table = 'users';
        protected $classNamespace = __NAMESPACE__;
    }