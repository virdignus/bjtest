<?php

    namespace App\Models;

    use Core\Model;

    class Task extends Model
    {
        protected $table = 'tasks';
        protected $classNamespace = __NAMESPACE__;
        protected $fillable = ['user_name','user_email','task_image','task_body'];

    }