<?php

    namespace App\Controllers;

    use App\Models\User;
    use Core\View;
    use App\Models\Task;


    class IndexController
    {

        /**
         *  show index page
         */
        public function index()
        {
            session_start();

            $errors = [];
            $success = "";

            if (isset($_SESSION['errors'])) {
                $errors = $_SESSION['errors'];
                unset($_SESSION['errors']);
            }
            if (isset($_SESSION['success'])) {
                $success = $_SESSION['success'];
                unset($_SESSION['success']);
            }

            $page = 0;
            $limit = 3;
            $orderBy = 'id';
            $orderType = 'DESC';

            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }
            if (isset($_GET['sortBy'])) {
                if (($_GET['sortBy'] == 'user')) {
                    $orderBy = 'user_name';
                } elseif ($_GET['sortBy'] == 'email') {
                    $orderBy = 'user_email';
                } elseif ($_GET['sortBy'] == 'status') {
                    $orderBy = 'done';
                }

            }

            $tasks = new Task();

            echo View::view('index', [
                'tasks'   => $tasks->paginate($limit, $page, $orderBy, $orderType),
                'errors'  => $errors,
                'success' => $success,
                'pages'   => $tasks->pagesCount($limit),
                'user'    => (new User)->checkAuth()
            ]);
        }
    }