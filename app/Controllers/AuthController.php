<?php

    namespace App\Controllers;


    use App\Models\User;
    use Core\Route;

    class AuthController
    {

        /**
         * Login user in application
         */
        public function login()
        {
            $login = $_POST['login'];
            $password = $_POST['password'];

            $user = (new User())->login($login, $password);
            if (empty($user)) {
                session_start();
                $_SESSION['errors'][] = "Auth: Incorrect user or password";

                return Route::redirectToIndexPage();
            }
            session_start();
            $_SESSION['success'] = "Welcome back $user->login";

            return Route::redirectToIndexPage();


        }

        /**
         * Log out user
         */
        public function logout()
        {
            setcookie('id');
            setcookie('session');

            return Route::redirectToIndexPage();
        }
    }