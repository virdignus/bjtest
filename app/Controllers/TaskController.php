<?php

    namespace App\Controllers;

    use App\Models\Task;
    use App\Models\User;
    use Core\Application;
    use Core\Route;
    use Core\View;

    class TaskController
    {
        /**
         * Add new task
         */
        public function store()
        {
            if (empty($_POST)) {
                return Route::redirectToIndexPage();
            }

            $task = new Task();
            $task->user_name = htmlentities($_POST['user_name']);
            $task->user_email = htmlentities($_POST['user_email']);
            $task->task_body = htmlentities($_POST['task_body']);
            $imagePath = Application::DEFAULT_IMAGE;

            if (!empty($_FILES['task_image']['name'])) {
                $imagePath = Application::uploadImage($task->user_email, 'task_image');
                if (is_null($imagePath)) {
                    $imagePath = Application::DEFAULT_IMAGE;
                    session_start();
                    $_SESSION['errors'][] = "Selected file was not image. Default image was set.";
                }
                $task->task_image = $imagePath;
            }

            $task->task_image = $imagePath;

            $task->insert();

            session_start();
            $_SESSION['success'] = "Great!!! You Added a new task!!! ";

            return Route::redirectToIndexPage();

        }

        /**
         * Show task editing form
         */
        public function show()
        {
            $id = (integer)$_GET['id'];
            if (empty($id) or !is_integer($id)) {
                return Route::redirectToIndexPage();
            }
            $task = new Task();

            echo View::view('task-edit', [
                'task' => $task->byId($id),
                'user' => (new User)->checkAuth()
            ]);

        }

        /**
         * update task
         */
        public function update()
        {

            $taskData['task_body'] = $_POST['task_body'];
            $taskData['done'] = isset($_POST['task_status']) ? 1 : 0;
            $task = new Task();
            $task->id = (integer)$_POST['task_id'];
            $task->update($taskData);

            return Route::redirectToIndexPage();

        }
    }